var map = new Map("premiere");

var pnj = new Personnage("pnj.png",14,4, DIRECTION.BAS);
var joueur = new Personnage("CharsetRed1.png", 12, 2, DIRECTION.BAS);

map.addPersonnage(pnj);
map.addPersonnage(joueur);




window.onload = function() {
	
	var canvas = document.getElementById('canvas');
	var ctx = canvas.getContext('2d');
	
	canvas.width  = map.getLargeur() * 32;
	canvas.height = map.getHauteur() * 32;
	
	setInterval(function() {
		map.dessinerMap(ctx);
	}, 40);
	
	
	// Gestion du clavier
window.onkeydown = function(event) {
	var e = event || window.event;
	var key = e.which || e.keyCode;
	switch(key) {
		case 38 : case 122 : case 119 : case 90 : case 87 : // Flèche haut, z, w, Z, W
			joueur.deplacer(DIRECTION.HAUT, map);
			break;
		case 40 : case 115 : case 83 : // Flèche bas, s, S
			joueur.deplacer(DIRECTION.BAS, map);
			break;
		case 37 : case 113 : case 97 : case 81 : case 65 : // Flèche gauche, q, a, Q, A
			joueur.deplacer(DIRECTION.GAUCHE, map);
			break;
		case 39 : case 100 : case 68 : // Flèche droite, d, D
			joueur.deplacer(DIRECTION.DROITE, map);
			break;
		case 73:
			console.log('inventaire');
			break;
		case 80:
			console.log('pause');
			break;
		default : 
			// console.log(key);
			// Si la touche ne nous sert pas, nous n'avons aucune raison de bloquer son comportement normal.
			return true;
	}




}



}

function Map(nom) {
	
	// Création de l'objet XmlHttpRequest
    var xhr = getXMLHttpRequest();
    // Chargement du fichier
    xhr.open("GET", './maps/' + nom + '.json', false);
    xhr.send(null);
    if(xhr.readyState != 4 || (xhr.status != 200 && xhr.status != 0)) // Code == 0 en local
        throw new Error("Impossible de charger la carte nommée \"" + nom + "\" (code HTTP : " + xhr.status + ").");
    var mapJsonData = xhr.responseText;

    // Ici viendra le code que je vous présente ci-dessous
    // Analyse des données
var mapData = JSON.parse(mapJsonData);
this.tileset = new Tileset(mapData.tileset);
this.terrain = mapData.terrain;

// Liste des personnages présents sur le terrain.
this.personnages = new Array();

// Pour ajouter un personnage
Map.prototype.addPersonnage = function(perso) {
	this.personnages.push(perso);
}
console.log(this.personnages);


// Pour récupérer la taille (en tiles) de la carte
Map.prototype.getHauteur = function() {
    return this.terrain.length;
}
Map.prototype.getLargeur = function() {
    return this.terrain[0].length;
}
Map.prototype.dessinerMap = function(context) {
	for(var i = 0, l = this.terrain.length ; i < l ; i++) {
		var ligne = this.terrain[i];
		var y = i * 32;
		for(var j = 0, k = ligne.length ; j < k ; j++) {
			this.tileset.dessinerTile(ligne[j], context, j * 32, y);
		}
	}
	// Dessin des personnages
	for(var i = 0, l = this.personnages.length ; i < l ; i++) {
		this.personnages[i].dessinerPersonnage(context);
}

}


}

